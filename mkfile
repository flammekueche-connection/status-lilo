status.pdf:

%.pdf: %.md
    pandoc -Mfontfamily:merriweather -M fontfamilyoptions:sfdefault -Vgeometry:"top=2cm, bottom=1.5cm, left=1cm, right=1cm" -o $target $stem.md

