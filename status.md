---
vim: et ts=2 sts=2 sw=2
cf: https://strasbourg.linuxfr.org/lilo/statuts
title: Status de l'Association LILO (ou Linux Logistique)
---

## Article 1 - Constitution et dénomination de l'association


Il est fondé, entre les adhérents aux présents statuts, une association à but
non lucratif, dénommée __LILO__ régie par les articles 21 à 79 du Code Civil
local, maintenus en vigueur par la loi d'introduction de la législation civile
française du 1er juin 1924 ainsi que par les présents statuts.

Elle sera inscrite au registre des associations du Tribunal d'Instance de
Strasbourg.

Elle pourra également être appelée __Linux Logistique__

----

### Article 1.b - Clause de dissolution


L'article 1 (Constitution et dénomination de l'assocation) ainsi que le présent
article (1.b - Clause de dissolution) ne peuvent être changés ni retirés sans
entraîner une dissolution automatique de l'association empêchant ainsi le
changement des statuts vers une association à but lucratif.

----

## Article 2 - Objectifs et moyens


L'association a pour objet de promouvoir, populariser, et développer
directement ou indirectement, le logiciel libre, et en particulier les systèmes
d'exploitation libres, tel que Linux. Est considéré comme libre tout logiciel
disponible sous forme de code source, librement redistribuable et modifiable,
selon des termes décrits par le groupe « Open Source ».

Plus généralement l'association s'efforce de défendre les concepts fondamentaux
du logiciel libre, tels que l'accessibilité des spécifications et des codes
source, la standardisation des interfaces et des représentations de données, la
qualité des implémentations (portabilité, modularité, compatibilité, stabilité,
robustesse, ouverture, ...), le développement coopératif et l'entraide
mutuelle.

L'association poursuit ses objectifs principalement dans le cadre des
communautés francophones. La langue officielle de l'association est le
français. Elle peut engager toute action qu'elle jugera utile permettant de
réaliser les objectifs qu'elle s'est fixés. Notamment, elle s'attachera à
promouvoir, et éventuellement à aider, le travail d'autres associations,
entreprises, administrations et particuliers oeuvrant dans le domaine du
logiciel libre ou ayant des activités qu'elle considère utiles à son
développement. Elle pourra, si cela lui semble utile, s'associer à d'autres
associations ou fédérations poursuivant des buts similaires.

Sont considérés comme des moyens permettant d'atteindre les objectifs de
l'association, la mise en oeuvre de listes de diffusion électroniques, de
serveurs d'information (W3), de serveurs distribuant des logiciels libres,
l'organisation, le soutien ou la participation à des conférences, journées
d'information, expositions, opérations de promotion de logiciels libres ou de
standards ouverts ou de rencontres sur des thèmes connexes.

----

## Article 3 - Siège social de l'association

Le siège de l'association est situé à la Maison des Associations, 1a place des
Orphelins, 67000 Strasbourg, dans le département du Bas-Rhin. Il pourra être
transféré par décision prise à la majorité des voix du conseil
d'administration.

----

## Article 4 - Durée de l'association

L'association est constituée pour une durée illimitée. L'assemblée générale
peut délibérer de sa pérennité.

----

## Article 5 - Membres de l'association


L'association est composée de membres actifs et de membres associés. Le
règlement intérieur peut établir différentes catégories de membres actifs ou
associés, en fonction des aides apportées, des service rendus ou des dons faits
à l'association.

----

### Article 5.a - Membres actifs

Toute personne physique ou morale peut être membre actif de l'association, à
condition d'adhérer aux présents statuts et au règlement intérieur. Elle sera
alors membre pour l'année en cours.

Une personne morale membre actif de l'association peut se faire représenter par
une personne physique de son choix, membre ou non de l'association, pour les
assemblées générales. Seules les personnes physiques, membres actifs de
l'association, peuvent être élues au conseil d'administration.

----

### Article 5.b - Membres associés

L'association peut également s'associer avec d'autres membres.

Toute personne physique ou morale peut être membre associé de l'association à
la condition de respecter l'esprit et la lettre des présents statuts, et de
s'acquitter de la participation éventuellement prévue. Elle sera membre associé
soit pour l'année de son admission, soit pour telle ou telle opération
ponctuelle.

Un membre associé participera par ses connaissances, ses relations et ses dons,
au développement des activités de l'association. Il pourra participer aux
assemblées générales où il ne prendra cependant pas part aux votes.

----

## Article 6 - Conditions d'admission des membres

La qualité de membre est acquise sur demande écrite adressée au conseil
d'administration.

Les conditions d'admission sont fixées par le règlement intérieur. Le conseil
d'administration peut rejeter toute demande d'adhésion sans avoir à en préciser
le motif. Cette décision sera, si le demandeur non admis ne s'y oppose pas,
portée à la connaissance des membres lors de l'assemblée générale suivante.

----

## Article 7 - Perte de la qualité de membre


La qualité de membre se perd par démission adressée par écrit ou par courrier
électronique authentifié au président de l'association, le décès ou la
radiation prononcée par le conseil d'administration, pour infraction aux
présents statuts, pour non-respect du règlement intérieur ou pour motif grave
portant préjudice moral ou matériel à l'association.

L'intéressé est invité à fournir des explications écrites au conseil avant la
décision éventuelle de radiation. En cas d'urgence manifeste, le conseil pourra
procéder à la suspension de l'intéressé en attendant ses explications écrites.

Une décision d'exclusion devra être ratifiée par l'assemblée générale suivante.
Les personnes physiques ou morales perdant la qualité de membre de
l'association ne pourront agir au sein de l'association ou en son nom entre la
date de la décision du conseil et la décision de ratification par l'assemblée
générale suivante.

En cas de non-ratification par l'assemblée générale suivante, la radiation
prendra fin et l'intéressé sera déclaré à nouveau membre de plein droit.

----

## Article 8 - Assemblées générales


L'assemblée générale ordinaire est constituée par les membres actifs,
participants ou représentés. Elle se réunit en session ordinaire une fois par
an, et aussi souvent que nécessaire en session extraordinaire, sur la
convocation du président de l'association, à la demande de la majorité des
administrateurs ou à la demande d'au moins un tiers des membres actifs.

Les assemblées générales peuvent être organisées, au choix du conseil, soit
sous forme de réunions de personnes sur le territoire métropolitain, soit sous
forme de réunions à distance autorisant les mêmes possibilités de notification,
convocation, représentation, discussion et vote, conformément aux conditions
précisées par le règlement intérieur.

Elle fixe, ou donne pouvoir au conseil d'administration de fixer, le montant
annuel des cotisations et participations s'il y a lieu.

Les décisions de l'assemblée générale (ordinaire ou extraordinaire) sont prises
à la majorité des membres présents ou représentés.

Le vote par correspondance est possible dans les conditions définies au
règlement intérieur.

Pour que l'assemblée générale soit valablement constituée, le quorum, prenant
en compte les membres actifs présents, représentés ou votant par
correspondance, est fixé à 50 % du nombre total des adhérents.

En l'absence de quorum, une nouvelle assemblée générale sera tenue dans un
délai de trente jours maximum, et elle pourra alors délibérer sans quorum.

Les convocations sont faites au moins quinze jours à l'avance par notification
individuelle indiquant le jour et le lieu de la réunion et l'ordre du jour
dressé par le conseil d'administration. Il sera également précisé s'il s'agit
d'une réunion de personnes ou d'une réunion à distance.

Tout membre de l'association peut adresser au conseil d'administration, jusqu'à
sept jours avant la date de la réunion, une proposition d'inscription d'un
sujet à l'ordre du jour. Toute proposition faisant l'objet d'un vote positif
d'un quart au moins des membres de l'association participants ou représentés à
l'assemblée générale sera rajoutée à l'ordre du jour.

L'assemblée générale délibère alors sur tous les points inscrits à l'ordre du
jour ainsi complété.

En cas de partage, la voix du président est prépondérante. Tout membre actif
peut se faire représenter par un autre membre actif et muni d'un pouvoir comme
précisé dans le règlement intérieur. Un membre actif ne pourra détenir plus de
trois pouvoirs.

L'assemblée générale reçoit le rapport moral et le rapport financier de l'année
écoulée et statue sur leur approbation. Elle procède à l'élection du conseil
d'administration, selon la procédure prévue par le règlement intérieur.

----

## Article 9 - Le conseil d'administration


L'association est administrée par son conseil d'administration, chargé
d'assurer l'exécution des décisions prises en assemblée générale.

Il prépare le budget, rédige le compte rendu moral qui sera lu en assemblée
générale, fait procéder aux convocations des assemblées générales ordinaires et
extraordinaires, et arrête leur ordre du jour.

Il autorise toutes acquisitions, aliénations ou locations immobilières ainsi
que les contrats de toute sorte à intervenir entre l'association et des
personnes physiques ou des personnes morales, de droit public ou de droit
privé. En particulier, tout acte dont le montant dépasse une somme fixée par le
règlement intérieur sera l'objet d'un vote par le conseil d'administration à la
majorité simple.

Il oriente, coordonne et surveille les oeuvres des groupes de travail s'ils
existent.

Il assure le respect des statuts et du règlement intérieur, et d'une façon
générale, le bon fonctionnement de l'association.

Il statue sur les demandes d'adhésions et sur les exclusions éventuelles.

Les décisions du conseil d'administration sont prises à la majorité simple des
présents ou représentés. En cas de partage des voix pour quelque décision que
ce soit, celle du président est prépondérante.

Tout administrateur peut faire inscrire ce qu'il désire à l'ordre du jour du
conseil.

Tout membre du conseil d'administration est révocable à tout moment par
l'assemblée générale, réunie en session ordinaire ou extraordinaire.

Tout membre du conseil d'administration sera révoqué de plein droit après trois
absences non excusées à des réunions du conseil.

De même, tout membre du conseil d'administration absent physiquement et
électroniquement (courrier, téléphone, ...) à la moitié des réunions du conseil
dans l'année sera également révoqué de plein droit.

Des membres de l'association ou toute personne étrangère à celle-ci, peuvent
assister à des réunions du conseil d'administration à la demande de celui-ci,
dans la mesure où le conseil estime leur présence nécessaire de par leurs
fonctions. Ils n'ont que voix consultative, et ne participent donc pas aux
votes.

Nul ne peut faire partie du conseil d'administration s'il n'est pas majeur.

Le conseil est investi des pouvoirs les plus étendus pour décider et autoriser
toutes opérations ayant pour but la réalisation de l'objet de l'association
ainsi que pour gérer sa gestion, son développement et son fonctionnement.

Le conseil d'administration se réunit au moins tous les ans sur convocation du
président ou sur la demande de la moitié de ses membres.

Le président ayant signature sur tout document engageant la responsabilité de
l'association, il peut habiliter par écrit, tout membre du conseil, à signer
les documents comptables et financiers de l'association. Il représente
l'association dans tous les actes de la vie civile, et est investi des pouvoirs
à cet effet. Il conclut tout accord avec des personnes physiques ou morales
sous réserve des autorisations qu'il doit obtenir du conseil. A ce titre, il
passe les contrats au nom de l'association.

Le président a qualité pour présenter toute réclamation auprès de toutes
administrations, notamment en matière fiscale, et pour ouvrir tout compte
bancaire ou postal. Il agit en justice au nom de l'association, avec
l'autorisation du conseil tant en demande qu'en défense.

Toute délégation du pouvoir du président fera à chaque fois l'objet d'une
déclaration écrite communiquée au conseil.

En cas d'absence ou de maladie, il est remplacé par l'aîné des assesseurs qui
dispose alors des mêmes pouvoirs.

Le président dirige et convoque les réunions du conseil d'administration.

Le trésorier est chargé de tenir ou de faire tenir sous son contrôle la
comptabilité de l'association. Il est le seul, avec le président, à percevoir
les recettes et effectuer tout paiement (sous réserve de l'autorisation du
président). Il présente un arrêté des comptes annuels en assemblée générale. En
cas d'empêchement, le trésorier est remplacé par un autre membre du conseil
désigné par le président.

Le secrétaire est chargé de la correspondance de l'association et prépare
également les comptes rendus des réunions du conseil d'administration, ainsi
que l'ordre du jour des réunions à venir.

Le président du conseil est le président de l'association.

----

## Article 10 - Élection au conseil d'administration


L'association est dirigée par un conseil de membres élus pour trois années. Les
membres sont élus par cooptation des candidats sélectionnés par l'assemblée
générale de l'association. Le conseil comprend au moins sept et au plus quinze
membres. Les membres fondateurs de l'association forment le premier conseil.

En cas de vacance, le conseil pourvoit provisoirement au remplacement de ses
membres. L'assemblée générale la plus proche procède à leur remplacement
définitif. Les pouvoirs des membres ainsi élus prennent fin à l'époque où
devrait normalement expirer le mandat des membres remplacés.

Le conseil d'administration élit tous les deux ans, dans ses rangs :

  * un président,
  * un trésorier,
  * un secrétaire,
  * quatre assesseurs,

Tous les membres du conseil sont éligibles et rééligibles.

En cas de conflit entre le président et le conseil d'administration, une
réunion du conseil peut être provoquée par l'un quelconque des membres du
conseil d'administration, et l'élection d'un nouveau président mise à l'ordre
du jour.

En cas de partage des voix pour quelque décision que ce soit, celle du
président est prépondérante.

Le président sortant assure ses fonctions de président jusqu'à l'élection du
nouveau président.

----

## Article 11 - Gratuité du mandat


Les membres du conseil d'administration ne peuvent recevoir aucune rétribution
à raison des fonctions qui leur sont conférées. Ils pourront toutefois obtenir
le remboursement des dépenses engagées pour les besoins de l'association, sur
justification et après accord écrit du président.

----

## Article 12 - Ressources et cotisations


Les ressources de l'association sont constituées des dons, des subventions, des
cotisations éventuelles des membres (telles que fixées par le règlement
intérieur), des produits de son activité, ainsi que toutes autres ressources ou
subventions qui lui seraient accordées et qui ne seraient pas contraires aux
lois en vigueur.

C'est le conseil d'administration qui gère les finances de l'association au
mieux des intérêts de cette dernière.

----

## Article 13 - Participation des membres associés


Le conseil d'administration propose chaque année le montant minimal de la
participation des membres associés, et le fait approuver par l'assemblée
générale.

C'est également lui qui détermine le montant de la participation du membre
associé dans le cadre d'une opération ponctuelle telle que définie à l'article
5b ci-dessus.

----

## Article 14 - Communication interne


Les outils de communication modernes, tels le téléphone, le courrier
électronique ou les logiciels de travail en groupe, pourront être utilisés en
lieu et place du courrier traditionnel ou des rencontres directes pour
simplifier le travail du conseil d'administration, ainsi que pour la
communication entre ce dernier et les membres de l'association.

Ces moyens pourront en particulier être utilisés pour les réunions du conseil
d'administration, dans des conditions précisées par le règlement intérieur.

----

## Article 15 - Utilisation du logo de l'association


Les membres actifs ou associés peuvent faire référence à leur affiliation à
l'association, à condition d'en respecter les buts et la déontologie.

L'utilisation du ou des logos de l'association sur un document papier est
soumise expressément à l'accord du président. Sur un document hypermédia qui
respecte l'esprit et la lettre des statuts de l'association, elle est
subordonnée à l'existence d'un lien hypertexte du logo vers le site officiel de
l'association ou vers un miroir de ce site agréé par l'association.

----

## Article 16 - Représentation et prestation


Tout acte ou prestation effectué au bénéfice de tiers au nom de l'association
par l'un de ses membres devra être autorisé par le président. Si l'acte ou la
prestation au nom de l'association est rétribué, il ne pourra donner lieu à
rétribution personnelle, l'association étant dans ce cas le seul bénéficiaire
autorisé, en la personne de son trésorier.

La rémunération de prestations pour le compte de l'association doit être
autorisée par écrit par le président ou toute personne dûment mandatée par lui.

----

## Article 17 - Dissolution


La dissolution de l'association ne peut être prononcée que par une assemblée
générale extraordinaire, convoquée spécialement à cet effet.

Pour ce faire, une majorité des deux tiers des votants doit être obtenue. Un ou
plusieurs liquidateurs seront alors désignés par l'assemblée générale, qui
disposeront des actifs en faveur de la FSF ("Free Software Foundation").

----

# Article 18 - Statuts


Seule l'assemblée générale a le pouvoir de faire addition ou modification aux
présents statuts qui seront adoptés par elle.

Cette modification ne pourra intervenir qu'à la majorité des deux tiers des
membres votants.

----

# Article 19 - Règlement intérieur

Un règlement intérieur de l'association est établi et mis à jour par le conseil
d'administration. Il est en permanence tenu à la disposition des adhérents de
l'association. Il est établi en respect des présents statuts et a force
obligatoire à l'égard de tous les membres de l'association.

Le règlement est destiné à fixer les divers points non prévus par les statuts
et utiles à la réalisation des objectifs de l'association.

Les modifications, proposées par le conseil d'administration, sont soumises au
vote de l'assemblée générale. Cependant, en cas d'urgence, elles peuvent être
adoptées provisoirement jusqu'à leur ratification par la prochaine assemblée
générale, par un vote positif du conseil d'administration à la majorité des
deux tiers.

----

//Fait à Strasbourg le 3 décembre 1998 en 2 exemplaires originaux.//\\
//Membres fondateurs// :

> M. Cyril Chaboisseau, Président M. François Jeanmougin, Trésorier M. Guy
> Brand, Secrétaire M. Bruno Boettcher, Assesseur M. Stéphane Casset, Assesseur
> M. Marc Grunberg, Assesseur M. Joel Kociolek, Assesseur


